import os
import markdown
from datetime import datetime
from django.conf import settings


def get_file_list(dir_path):
    file_list = next(os.walk(dir_path))[2]
    file_paths = []
    for file in file_list:
        if file.endswith('.md'):
            file_paths.append(os.path.join(dir_path, file))
    return file_paths


def get_dir_list():
    dir_list = []
    for i in settings.NOTE_DIRS:
        note_dir = {
            'name': i['name'],
            'path': i['path'],
            'parent': None,
        }
        dir_list.append(note_dir)
        sub_dirs = next(os.walk(i['path']))[1]
        if len(sub_dirs) > 0:
            for dir in sub_dirs:
                sub_dir = {
                    'name': dir,
                    'path': i['path'] + '/' + dir,
                    'parent': i['name'],
                }
                dir_list.append(sub_dir)
    return dir_list


def read_file(file_path):
    f = open(file_path, 'r')
    md = markdown.Markdown(extensions=['markdown.extensions.meta'])
    path_file_name = os.path.split(file_path)
    dir_name = os.path.split(path_file_name[0])[1]
    body = md.convert(f.read())
    meta = md.Meta
    md.reset()
    f.close()
    return {
        'title': meta['title'][0],
        'dir': dir_name,
        'file': path_file_name[1],
        'url': '/' + dir_name + '/' + path_file_name[1],
        'date': datetime.strptime(meta['date'][0], '%Y.%m.%d').date(),
        'body': body,
    }


def get_file_dict(paths):
    file_dict = []
    for path in paths:
        file_list = get_file_list(path['path'])
        for file_path in file_list:
            file_dict.append(read_file(file_path))
    file_dict = sorted(file_dict, key=lambda k: k['date'], reverse=True)
    return file_dict
