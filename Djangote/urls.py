from django.urls import path
from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name="index"),
    path('<d_name>/<f_name>/', views.NoteView.as_view(), name="note")
]
