import os
from django.views import generic
from django.http import Http404
from django.shortcuts import render
from .utils import get_file_dict, get_dir_list, read_file


class IndexView(generic.ListView):
    template_name = "index.html"
    context_object_name = "notes"
    paginate_by = 8

    def get_queryset(self):
        dirs = get_dir_list()
        return get_file_dict(dirs)


class NoteView(generic.DetailView):
    template_name = "note.html"
    context_object_name = "note"

    def get_object(self):
        dirs = get_dir_list()
        for i in dirs:
            if i['name'] == self.kwargs['d_name']:
                note_dir = i
                break
        return read_file(note_dir['path'] + '/' + self.kwargs['f_name'])
